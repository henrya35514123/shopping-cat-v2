# 選擇 image
FROM denoland/deno:alpine-1.37.2

# 建立資料夾
WORKDIR /app

# 複製專案到 app 資料夾中
# 會忽略 .dockerignore 裡的檔案
COPY . /app

# 開放 container 的 8000 port
EXPOSE 8000

# deno 指令
RUN deno cache main.ts

# 執行 main.ts
CMD ["run", "--allow-all", "main.ts"]
